-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Erstellungszeit: 29. Dez 2012 um 10:50
-- Server Version: 5.5.27
-- PHP-Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `webseite`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `indoor`
--

CREATE TABLE IF NOT EXISTS `indoor` (
  `Datum` datetime NOT NULL,
  `Strecke` decimal(3,1) NOT NULL,
  `Zeit` time NOT NULL,
  `Kcal` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `outdoor`
--

CREATE TABLE IF NOT EXISTS `outdoor` (
  `Datum` datetime NOT NULL,
  `Strecke` decimal(2,0) NOT NULL,
  `Zeit` time NOT NULL,
  `Kcal` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Daten für Tabelle `outdoor`
--

INSERT INTO `outdoor` (`Datum`, `Strecke`, `Zeit`, `Kcal`) VALUES
('2012-12-29 10:42:46', 11, '01:12:00', 712);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
